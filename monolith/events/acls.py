from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    url = 'https://api.pexels.com/v1/search'
    params = {'query': f'{city} {state}'}
    headers = { "Authorization": PEXELS_API_KEY, }
    response = requests.get(url, params=params, headers=headers)

    content = response.json()

    return {
        "picture_url": content["photos"][0]["src"]["original"]
    }
